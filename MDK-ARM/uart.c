#include <stm32f4xx.h>
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "uart.h"
#include "misc.h"
NVIC_InitTypeDef   NVICinit;


 #ifdef __GNUC__
   /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
       set to 'Yes') calls __io_putchar() */
    #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
 #else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
    #define GETCHAR_PROTOTYPE int fgetc(FILE *f)
 #endif /* __GNUC__ */

void GPIO_init(void)
{
	GPIO_InitTypeDef GPIOinitstruct;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	GPIOinitstruct.GPIO_Mode = GPIO_Mode_AF;
	GPIOinitstruct.GPIO_OType = GPIO_OType_PP;
	GPIOinitstruct.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;
	GPIOinitstruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIOinitstruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOB,&GPIOinitstruct);
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource6,GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource7,GPIO_AF_USART2);
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
	GPIOinitstruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIOinitstruct.GPIO_OType = GPIO_OType_PP;
	GPIOinitstruct.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
	GPIOinitstruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIOinitstruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOD,&GPIOinitstruct);
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); 
	GPIOinitstruct.GPIO_Mode = GPIO_Mode_OUT; 
	GPIOinitstruct.GPIO_OType = GPIO_OType_PP;
	GPIOinitstruct.GPIO_Pin = GPIO_Pin_5; 
	GPIOinitstruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIOinitstruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIOinitstruct);
	
  /* PD14 and PD15 for motor1
	PD12 PD13 for motor2*/
}

void UART_init(void)
{
	USART_InitTypeDef UARTinitstruct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE); 
	UARTinitstruct.USART_BaudRate = 9600;
	UARTinitstruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	UARTinitstruct.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
	UARTinitstruct.USART_Parity = USART_Parity_No;
	UARTinitstruct.USART_StopBits = USART_StopBits_1;
	UARTinitstruct.USART_WordLength = USART_WordLength_8b;	
	USART_Init(USART1,&UARTinitstruct);
	USART_Cmd(USART1,ENABLE);
	
	NVICinit.NVIC_IRQChannel = USART1_IRQn ;
  NVICinit.NVIC_IRQChannelPreemptionPriority =0;
  NVICinit.NVIC_IRQChannelSubPriority=0;
  NVICinit.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVICinit);
  USART_ITConfig(USART1, USART_IT_RXNE,ENABLE); 
}

void USART2_Configuration(unsigned int baudRate)
{
	GPIO_InitTypeDef           GPIO_InitStructure;
  USART_InitTypeDef          USART_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	// enable DMA
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1,ENABLE);
  
  /* Configure USART Tx as alternate function  */
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  USART_InitStructure.USART_BaudRate = baudRate;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART2, &USART_InitStructure);
  
  GPIO_PinAFConfig(GPIOA,GPIO_PinSource2,GPIO_AF_USART2); 
  GPIO_PinAFConfig(GPIOA,GPIO_PinSource3,GPIO_AF_USART2); 
  
  USART_Cmd(USART2, ENABLE);  
	
	
	
	// ngat 
//	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn ;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority =1;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
//  USART_ITConfig(USART2, USART_IT_RXNE,ENABLE); 
}

void Send_uart1(uint16_t dataout)
{
	USART_SendData(USART1,dataout);
	while (USART_GetFlagStatus(USART1,USART_FLAG_TXE) == RESET) ;
	USART_ClearFlag(USART1,USART_FLAG_TXE);
}

uint16_t Get_uart1(void)
{
	while (USART_GetFlagStatus(USART1,USART_FLAG_RXNE) == RESET) ;
	USART_ClearFlag(USART1,USART_FLAG_RXNE);
	return USART_ReceiveData(USART1);
}
void USART_puts(const char *s, uint8_t n){
	uint8_t i;
	for(i = 0; i < n; i++)
	{
		Send_uart1( *s);
		s++;
	}
}



