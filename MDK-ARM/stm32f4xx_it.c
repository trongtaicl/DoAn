/**
  ******************************************************************************
  * @file    TIM/PWM_Output/stm32f4xx_it.c 
  * @author  MCD Application Team
  * @version V1.0.1
  * @date    13-April-2012
  * @brief   Main Interrupt Service Routines.
  *         This file provides template for all exceptions handler and
  *         peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include "motor.h"
#include "math.h"
#include "uart.h"
#include "stdlib.h"
#include "tm_stm32f4_mpu6050.h"
#include "config.h"


extern Motor_t* pMotor1;
extern Motor_t motor1;
extern Motor_t* pMotor2;
extern Motor_t motor2; 
extern Robot_t  robot; 
extern Robot_t* pRobot; 
extern MPU6050_Device_t MPU6050Dev; 

extern int start; 
extern uint8_t charReceived; 

//extern float bufferMotor1[2];
//extern float bufferMotor2[2]; 
//extern uint8_t reset; 
//extern uint8_t resetCount; 
//extern uint8_t state; 
extern uint32_t ENCCount; 
//extern uint8_t  txBuffer[3]; 
extern uint16_t  txCounter;
//extern uint8_t  rxBuffer[3]; 
extern uint16_t rxCounter ; 

extern float  speedMotor1;
extern float  speedMotor2; 
extern uint16_t counterTIM2; 
extern uint16_t counterTIM1; 
volatile extern uint8_t test; 
/** @addtogroup STM32F4xx_StdPeriph_Examples
  * @{
  */

/** @addtogroup TIM_PWM_Output
  * @{
  */ 

void sendPositionToMotor(float bufferMotor1[], float bufferMotor2[], uint16_t lengthBuffer);
static void initialize_motor(Motor_t* pMotor, uint16_t thresh);
static void comparePWM(Motor_t* motor1, Motor_t* motor2); 
static void motor_CalculatePos(Motor_t* motor1, Motor_t* motor2, Robot_t* robot, MPU6050_Device_t MPU6050);

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/
    
  
/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
//void HardFault_Handler(void)
//{
  /* Go to infinite loop when Hard Fault exception occurs */
  //while (1)
  //{}
//}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {}
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {}
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {}
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{}

/**
  * @brief  This function handles PendSV_Handler exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{

}

void TIM2_IRQHandler(void)
{
	
}

/****** usart1 interrupt ************/
void USART1_IRQHandler(void)
{ 
	uint16_t charTemp =0;
  uint8_t  i =0, num =0;
 	uint8_t *numPoint = &num; 
	
	/**********
	if receive data 
	***********/
	if(USART_GetITStatus(USART1,USART_IT_RXNE)== SET)
	{  
//		rxBuffer[rxCounter++] = (USART_ReceiveData(USART1) &0xff); // save data to a buffer
		//each time, receive 4 bytes. 
		if (rxCounter ==4)  
		{
			rxCounter = 0; 
			// save 4 bytes to a pointer. 
			for(i =0; i< 4; i++)
			{
	//			*(numPoint++) = (uint8_t)rxBuffer[i];
			}
			
			pRobot->xPos = *((float*)&num); // chuyen dia chi qua float, roi lay gia tri cua cai float do.  
			
			USART_ITConfig(USART1, USART_IT_TXE, ENABLE); 
		}
		USART_ClearITPendingBit(USART1,USART_IT_RXNE);
   }
	/***
	if start to send data, each time, buffer is filled with data, and enable TXE_IT flag. */
	if (USART_GetITStatus(USART1, USART_IT_TXE) == SET)
	{
//		USART_SendData(USART1, txBuffer[txCounter++]);
		if (txCounter == 3)
		{
			txCounter = 0; 
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE); 
			USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 
		}
	}
}


void TIM4_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM4,TIM_IT_Update) != RESET)
	{ 
				if (start ==1)
				{	 //test wires and motors			
					/*GPIO_SetBits(pMotor1->pGPIO,pMotor1->pGPIOForward);
					GPIO_ResetBits(pMotor1->pGPIO,pMotor1->pGPIOReverse);
					GPIO_SetBits(pMotor2->pGPIO, pMotor2->pGPIOForward);
					GPIO_ResetBits(pMotor2->pGPIO, pMotor2->pGPIOReverse); 
					*pMotor2->pTimCCR = 0;
					*pMotor1->pTimCCR = 8000; */
					motor_sampling(pMotor1);
          					
					motor_control(pMotor1,0.1); 
		      //ENCCount += 1; 
						/* Set the Autoreload value */		
				}	
				else if (start ==3)
				{
					//ENCCount = TIM_GetCounter(TIM5); 
					motor_sampling(pMotor2);
					motor_sampling(pMotor1); 
					motor_CalculatePos(pMotor1, pMotor2, pRobot, MPU6050Dev); 
					
					GPIO_SetBits(pMotor1->pGPIO,pMotor1->pGPIOForward);
					GPIO_ResetBits(pMotor1->pGPIO,pMotor1->pGPIOReverse);
					GPIO_SetBits(pMotor2->pGPIO, pMotor2->pGPIOForward);
					GPIO_ResetBits(pMotor2->pGPIO, pMotor2->pGPIOReverse);
					*pMotor2->pTimCCR = 6500; 
					*pMotor1->pTimCCR = 6200; 
					//ENCCount += 10; 
				}
				else
				{
					GPIO_SetBits(pMotor1->pGPIO,pMotor1->pGPIOForward);
					GPIO_SetBits(pMotor1->pGPIO,pMotor1->pGPIOReverse);
					GPIO_SetBits(pMotor2->pGPIO, pMotor2->pGPIOForward);
					GPIO_SetBits(pMotor2->pGPIO, pMotor2->pGPIOReverse);
					*pMotor2->pTimCCR = 00; 
					*pMotor1->pTimCCR = 00; 
				}
				 
		}
	TIM_ClearITPendingBit(TIM4,TIM_IT_Update);
}



void sendPositionToMotor(float bufferMotor1[], float bufferMotor2[], uint16_t lengthBuffer)
{
	uint16_t j =0; 
	static uint8_t countVar=0; 
	
	if (!start)
	{
		countVar =0; 
	}
	
	if (countVar < lengthBuffer)
	{

	
		if (countVar ==0)
		{
			pMotor1->positionParams.setPosition = bufferMotor1[countVar];
      pMotor2->positionParams.setPosition = bufferMotor2[countVar];
//      reset =0; 		
      countVar++;			
		}
		
		
	}
}

static void comparePWM(Motor_t* motor1, Motor_t* motor2)
{
	if (motor1->PulseW != motor2->PulseW)
	{
		motor2->PulseW = motor1->PulseW; 
	}
}

void motor_sampling(Motor_t* pMotor)
	{	
	// Update stat
		
	      int    numTemp =0; 
	      static uint16_t numReset =0; 
				if((pMotor->speedParams.currentSpeed < pMotor->speedParams.startRPM)
					|| (pMotor->speedParams.currentSpeed > -pMotor->speedParams.startRPM))
				{
					pMotor->state = motor_state_idle;		
					pMotor->profile.motor_idleTime = 50; 
				}
			  if((pMotor->speedParams.currentSpeed >= pMotor->speedParams.startRPM)
					||(pMotor->speedParams.currentSpeed <= -pMotor->speedParams.startRPM))
				{
					pMotor->state = motor_state_running;
					pMotor->profile.motor_idleTime = 0;
				}

	// Sampling Encoder
		
			pMotor->ENC_count[2] = pMotor->ENC_count[1];
			pMotor->ENC_count[1] = pMotor->ENC_count[0];
			pMotor->ENC_count[0] = *pMotor->pTimCNT; 
      numTemp              = pMotor->ENC_count[0] - pMotor->ENC_count[1]; 					
			
			
			if (abs(pMotor->ENC_count[0] - pMotor->ENC_count[1]) > 1000)
			{
				  pMotor->speedParams.currentPulse = 0;		
          pMotor->test += 100; 				
			}
			
			else if (abs(pMotor->ENC_count[0] - pMotor->ENC_count[1]) < 1000)
			{
				pMotor->speedParams.currentPulse = pMotor->ENC_count[0] - pMotor->ENC_count[1]; 
			}
			
			pMotor->positionParams.currentPosition += pMotor->speedParams.currentPulse; 
			
			pMotor->speedParams.currentSpeed = (float)pMotor->speedParams.currentPulse*pMotor->speedParams.speedCOF;
			pMotor->speedParams.speedCms     = (float)pMotor->speedParams.currentSpeed*pMotor->speedParams.convertRPMToCms; 
			
			if (pMotor == pMotor1)
			{
				speedMotor1 = pMotor1->speedParams.currentSpeed;
			}
      else if (pMotor == pMotor2)
			{
				speedMotor2 = pMotor2->speedParams.currentSpeed;
			}				
			
			
			if ((pMotor->speedParams.speedCms > 2000) || (pMotor->speedParams.speedCms < -2000))
			{
				pMotor->speedParams.speedCms = 0; 
			}
			else
			{
				pMotor->speedParams.speedCms = pMotor->speedParams.speedCms;
			}
	/*// Calculate position		
			if ((pMotor->positionParams.setPosition >0) && (numTemp >0x07530))
			{ // trong truong hop goc setposition nho, co the dong co bi quay lai, gia tri timer doc ve lon. 
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0] - 0x0FFFFFFFF;
				
			}
			
			else if (pMotor->positionParams.setPosition >0)
			{
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0]; 
				
			}
			
		 if ((pMotor->positionParams.setPosition < 0) && ((-numTemp) > 0x07530))
			{
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0]; 
			}
			
			else if ((pMotor->positionParams.setPosition < 0) && (pMotor->ENC_count[0] < 5000))
			{ // ban dau o trang thai nghi sang trang thai quay, de phong. 
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0]; 
			}
			else if (pMotor->positionParams.setPosition < 0)
			{
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0] - 0x0FFFFFFFF; 
							
			}
			
			if (abs(pMotor->positionParams.currentPulse) >(uint32_t)0xFFFFF)
			{
				pMotor->state = motor_state_break;
			}
			else
			{
				pMotor->state = motor_state_running;
			}
			pMotor->positionParams.currentPosition    = (float)pMotor->positionParams.currentPulse*pMotor->positionParams.positionEOF; 
*/			
}
	
static void motor_CalculatePos(Motor_t* motor1, Motor_t* motor2, Robot_t* robot, MPU6050_Device_t MPU6050)
{
	float speedRobot = 0; 
	float thetaRadian; 
	float speedX, speedY; 
	float thetaTmp; 
	
	if (MPU6050.angle_roll >180)
	{
		thetaTmp = MPU6050.angle_roll - 360; 
	}
	else if (MPU6050.angle_roll < -180)
	{
		thetaTmp = MPU6050.angle_roll + 360; 
	}
	else
	{
		thetaTmp = MPU6050.angle_roll; 
	}
	
	thetaRadian = thetaTmp*0.0174533f; 
	speedX = (float)(motor1->speedParams.speedCms+ motor2->speedParams.speedCms)*0.5*cos(thetaRadian); 
	
	speedY = (float)(motor1->speedParams.speedCms+ motor2->speedParams.speedCms)*0.5*sin(thetaRadian);
	
	robot->xPos += speedX*0.022;  
	robot->yPos += speedY*0.023; 
	robot->xPos = robot->xPos;
	robot->yPos = robot->yPos;
}

void motor_control(Motor_t* pMotor, float tSample)
{
		switch(pMotor->controlMode)
		{
			case pulseToSpeed:
				calculatePID(pMotor, tSample);
				break;
			case pulseToPosition:
				Motor_Position_PID(pMotor);
				break;
			case freeRun:
				break;
		}
}	


/**
*
(*/
void initialize_motor(Motor_t* pMotor, uint16_t thresh)
{
		int    numTemp =0; 
		
		pMotor->ENC_count[2] = pMotor->ENC_count[1];
		pMotor->ENC_count[1] = pMotor->ENC_count[0];
		pMotor->ENC_count[0] = *pMotor->pTimCNT; 
    numTemp              = pMotor->ENC_count[0] - pMotor->ENC_count[1]; 			
		
		
		// Calculate position		
			if ((pMotor->positionParams.setPosition >0) && (numTemp >0x07530))
			{ // trong truong hop goc setposition nho, co the dong co bi quay lai, gia tri timer doc ve lon. 
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0] - 0x0FFFFFFFF;
				
			}
			
			else if (pMotor->positionParams.setPosition >0)
			{
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0]; 
				
			}
			
		 if ((pMotor->positionParams.setPosition < 0) && ((-numTemp) > 0x07530))
			{
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0]; 
			}
			
			else if ((pMotor->positionParams.setPosition < 0) && (pMotor->ENC_count[0] < 5000))
			{ // ban dau o trang thai nghi sang trang thai quay, de phong. 
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0]; 
			}
			else if (pMotor->positionParams.setPosition < 0)
			{
				pMotor->positionParams.currentPulse = pMotor->ENC_count[0] - 0x0FFFFFFFF; 
							
			}
			
			pMotor->positionParams.p_error[2] = pMotor->positionParams.p_error[1];
			pMotor->positionParams.p_error[1] = pMotor->positionParams.p_error[0];
			pMotor->positionParams.p_error[0] = (float)pMotor->positionParams.setPosition/pMotor->positionParams.positionEOF - \
																								(float)pMotor->positionParams.currentPulse;
			pMotor->positionParams.currentPosition    = (float)pMotor->positionParams.currentPulse*pMotor->positionParams.positionEOF; 
			
			GPIO_SetBits(pMotor->pGPIO, pMotor->pGPIOForward); 
		  GPIO_ResetBits(pMotor->pGPIO, pMotor->pGPIOReverse); 
		
	   *pMotor->pTimCCR = thresh; 
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
