#include "motor.h"
#include "math.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"

/********************************************************************************
* Function prototypes
********************************************************************************/
extern uint32_t ENCCount; 


volatile extern uint8_t test; 
void Motor_Speed_PID(Motor_t* pMotor);
void Motor_Position_PID(Motor_t* pMotor);

/********************************************************************************
Local functions
********************************************************************************/

void calculatePID(Motor_t* pMotor, float tSample)
{
	float error1 = 0; 
  
	pMotor->speedParams.v_error[2] = pMotor->speedParams.v_error[1];
	pMotor->speedParams.v_error[1] = pMotor->speedParams.v_error[0];
	pMotor->speedParams.v_error[0] = pMotor->speedParams.setPulse - pMotor->speedParams.currentPulse;
	
	if ((pMotor->speedParams.setPulse * pMotor->speedParams.currentPulse < 0) || 
		((pMotor->speedParams.setSpeed == 0) && (pMotor->speedParams.currentSpeed != 0)))
		{
			pMotor->state = motor_state_break;
		}
		
	// Start Mode
	if(pMotor->state == motor_state_idle)
	{
		if(pMotor->speedParams.setSpeed>0)
		{
			pMotor->PulseW = pMotor->profile.startPulse;
		}
		else
		{
			pMotor->PulseW = -pMotor->profile.startPulse;
		}
		
		loadPulseW(pMotor);
	}
	else if((pMotor->state == motor_state_running) && (pMotor->speedParams.setSpeed != 0))
	{	
		float pPart, iPart, dPart; 		
		
		
		pMotor->pidSelf.pE = (float)pMotor->speedParams.v_error[0]*100.0f/(float)pMotor->speedParams.setPulse; 
		
		if (pMotor->pidSelf.pE > 4.0f||pMotor->pidSelf.pE<-4.0f)
		{
			pMotor->pidSelf.nuyKp = 1.0f;
			pMotor->pidSelf.nuyKi = 0.0f;
		}
		else if (pMotor->pidSelf.pE > 1.0f || pMotor->pidSelf.pE < -1.0f)
		{
			pMotor->pidSelf.nuyKp = 1.0f;
			pMotor->pidSelf.nuyKi = 0.0f;
		}
		else
		{
			pMotor->pidSelf.nuyKp = 1.0f;
			pMotor->pidSelf.nuyKi = 0.0f;
		}
		
		
		pMotor->speedParams.Kp = (float)(pMotor->speedParams.Kp + 
			                               pMotor->pidSelf.vP1*pMotor->speedParams.v_error[0]
		                                 *pMotor->pidSelf.nuyKp);
		pMotor->speedParams.Ki = (float)(pMotor->speedParams.Ki + 
			                               pMotor->pidSelf.vI1*pMotor->speedParams.v_error[0]
		                                 *pMotor->pidSelf.nuyKi);
		
		if (pMotor->speedParams.Kp > 500)
		{
			pMotor->speedParams.Kp = 500; 
		}
		else if (pMotor->speedParams.Kp < 0)
		{
			pMotor->speedParams.Kp = 0; 
		}
		if (pMotor->speedParams.Ki > 100)
		{
			pMotor->speedParams.Ki = 100; 
		}
		else if (pMotor->speedParams.Ki < 0)
		{
			pMotor->speedParams.Ki = 0; 
		}
		
		pPart = pMotor->speedParams.Kp 
						* (pMotor->speedParams.v_error[0]);
		pMotor->speedParams.iPart += pMotor->speedParams.Ki 
																	* tSample 							// Sample Period = 0.03
																	/ 2 
																	* (pMotor->speedParams.v_error[0] + pMotor->speedParams.v_error[1]) ;															
		//iPart = AntiWindup(iPart,100,-100);
		
		dPart = (pMotor->speedParams.Kd / tSample) 
							* (pMotor->speedParams.v_error[0] - 2 * pMotor->speedParams.v_error[1] + pMotor->speedParams.v_error[2]);
		
		pMotor->PulseW += (int)(pPart + iPart);
		
		// Anti-Windup for pulseW
		if (pMotor->speedParams.setPulse > 0 && pMotor->speedParams.currentPulse > 0)
		{
			pMotor->PulseW = AntiWindup(pMotor->PulseW,60000,0);
		}
		
		if (pMotor->speedParams.setPulse < 0 && pMotor->speedParams.currentPulse < 0)
		{
			pMotor->PulseW = AntiWindup(pMotor->PulseW, 0, -60000); 
		}
		// Anti-Windup for pMotor->PulseW
		
		loadPulseW(pMotor);
	}
	else if (pMotor->state == motor_state_break)
	{
		pMotor->PulseW = 0;
		loadPulseW(pMotor); 
	}
	
}

void initMotorAgain(Motor_t *Motor)
{
	Motor->positionParams.p_error[0] =0.0;
	Motor->positionParams.p_error[1] =0.0;
	Motor->positionParams.p_error[2] = 0.0;
	Motor->ENC_count[0] = 0;
	Motor->ENC_count[1] = 0;
	Motor->ENC_count[2] = 0; 
	Motor->positionParams.currentPulse =0; 
	Motor->PulseW       =0; 
	*(Motor->pTimCNT)   =0;
  *(Motor->pTimCCR)    = 0; 
}

static float AntiWindup(float val, float uBound, float lBound)
{
	if(val>=uBound) val = uBound;
	if(val<=lBound) val = lBound;

	return val;
}

void loadPulseW(Motor_t* pMotor)
{
	//valueCount2 = pMotor->PulseW;
	if ((pMotor->PulseW > 0))
	{
		pMotor->PulseW = AntiWindup(pMotor->PulseW, 13000, 0); 
		GPIO_SetBits(pMotor->pGPIO, pMotor->pGPIOForward); 
		GPIO_ResetBits(pMotor->pGPIO, pMotor->pGPIOReverse); 
		
	  *pMotor->pTimCCR = pMotor->PulseW;  
	   
	}
	else if (pMotor->PulseW <0)
	{
		pMotor->PulseW = AntiWindup(pMotor->PulseW, 0, -13000); 
		GPIO_SetBits(pMotor->pGPIO, pMotor->pGPIOReverse);
		GPIO_ResetBits(pMotor->pGPIO, pMotor->pGPIOForward); 
		*pMotor->pTimCCR = -pMotor->PulseW;  
	}
	else
	{
		GPIO_SetBits(pMotor->pGPIO, pMotor->pGPIOReverse);
		GPIO_SetBits(pMotor->pGPIO, pMotor->pGPIOForward); 
		*pMotor->pTimCCR = 0; 
	}
}	

void Motor_Speed_PID(Motor_t* pMotor) 
{
	pMotor->speedParams.v_error[2] = pMotor->speedParams.v_error[1];
	pMotor->speedParams.v_error[1] = pMotor->speedParams.v_error[0];
	pMotor->speedParams.v_error[0] = pMotor->speedParams.setPulse - pMotor->speedParams.currentPulse;
	
	if ((pMotor->speedParams.setPulse * pMotor->speedParams.currentPulse < 0) || 
		((pMotor->speedParams.setSpeed == 0) && (pMotor->speedParams.currentSpeed != 0)))
		{
			pMotor->state = motor_state_break;
		}
		
	// Start Mode
	if(pMotor->state == motor_state_idle)
	{
		if(pMotor->speedParams.setSpeed>0)
		{
			pMotor->PulseW = pMotor->profile.startPulse;
		}
		else
		{
			pMotor->PulseW = -pMotor->profile.startPulse;
		}
		
		loadPulseW(pMotor);
	}
	else if(pMotor->state == motor_state_running)
	{	
		float pPart, iPart, dPart; 		
		pPart = pMotor->speedParams.Kp 
						* (pMotor->speedParams.v_error[0]);
		pMotor->speedParams.iPart += pMotor->speedParams.Ki 
																	* (0.03F) 							// Sample Period = 0.03
																	/ 2 
																	* (pMotor->speedParams.v_error[0] + pMotor->speedParams.v_error[1]) ;															
		//iPart = AntiWindup(iPart,100,-100);
		
		dPart = (pMotor->speedParams.Kd / (0.03F)) 
						* (pMotor->speedParams.v_error[0] - 2 * pMotor->speedParams.v_error[1] + pMotor->speedParams.v_error[2]) ;
		
		pMotor->PulseW += (int)(pPart + iPart + dPart);
		
		// Anti-Windup for pulseW
		if (pMotor->speedParams.setPulse > 0 && pMotor->speedParams.currentPulse > 0)
		{
			pMotor->PulseW = AntiWindup(pMotor->PulseW,60000,0);
		}
		
		if (pMotor->speedParams.setPulse < 0 && pMotor->speedParams.currentPulse < 0)
		{
			pMotor->PulseW = AntiWindup(pMotor->PulseW, 0, -60000); 
		}
		// Anti-Windup for pMotor->PulseW
		
		loadPulseW(pMotor);
	}
	else if (pMotor->state == motor_state_break)
	{
		pMotor->PulseW = 0;
		loadPulseW(pMotor); 
	}
}
	
void Motor_Init(Motor_t* pMotor,
									Speed_control_t speedParams, Position_control_t positionParams, 
									motor_profile_t profile, pid_selfTurning_t pidParams)
	{
				pMotor->speedParams = speedParams;
				pMotor->positionParams = positionParams;
				pMotor->profile = profile;
		    pMotor->pidSelf = pidParams;
	}
	
void Motor_Position_PID(Motor_t* pMotor)
	{
			uint32_t topPulse =0;
			uint32_t basePulse =0;
			float abs_p_error0 =0;
			float pPart,dPart, iPartTemp;
		
		  if (pMotor->state == motor_state_running)
			{					   					
						// Error in Pulse
						pMotor->positionParams.p_error[2] = pMotor->positionParams.p_error[1];
						pMotor->positionParams.p_error[1] = pMotor->positionParams.p_error[0];
						pMotor->positionParams.p_error[0] = (float)pMotor->positionParams.setPosition/pMotor->positionParams.positionEOF - 
																								(float)pMotor->positionParams.currentPulse;
						
					//	valueCount = pMotor->positionParams.p_error[0]; 
						abs_p_error0 = fabs(pMotor->positionParams.p_error[0]);
						// 1260 <-> 300 deg
						// 840 	<-> 200 deg
						// 420 	<-> 100 deg
						// 210 	<-> 50 deg
						// 105 	<-> 25 deg
						pMotor->befPulseW = pMotor->PulseW; 
				
						if (pMotor->positionParams.setPosition !=0)
						{
								pMotor->positionParams.pPart =  pMotor->positionParams.Kp*
																								(pMotor->positionParams.p_error[0]);
							 
							//if ((pMotor->positionParams.p_error[0] <0) &&(pMotor->positionParams.iPart >0))
							//{
							//	iPartTemp = -pMotor->positionParams.iPart; 
						//	}
						//	else
						//	{
								
						//	}
							
								pMotor->positionParams.iPart += pMotor->positionParams.Ki*0.03F/2
																								*(pMotor->positionParams.p_error[0] + pMotor->positionParams.p_error[1]);

								pMotor->positionParams.dPart = (pMotor->positionParams.Kd/0.03F)
												*(pMotor->positionParams.p_error[0] - 2*pMotor->positionParams.p_error[1] + pMotor->positionParams.p_error[2]) ;
										
								pMotor->PulseW = pMotor->positionParams.pPart + pMotor->positionParams.iPart 
																 + pMotor->positionParams.dPart;	
							//	valueCount = pMotor->positionParams.p_error[2];
								
							/*	if ((pMotor->positionParams.setPosition >0) && (pMotor->PulseW < 0))
								{
									delay_01ms(10); 
								}
								
								if ((pMotor->positionParams.setPosition < 0) && (pMotor->PulseW >0))
								{
									delay_01ms(10); 
								} */
						
								pMotor->PulseW = AntiWindup(pMotor->PulseW, 15000, -15000);
								    					
								}
				
							else
							{
								pMotor->PulseW = 0; 
							}
						  }
					//		}
							else
							{
								pMotor->PulseW = 0 ;
							}				
	}
	
	void delay_01ms(uint16_t period){

  	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM12, ENABLE);
  	TIM12->PSC = 1600-1;		// clk = SystemCoreClock /2 /(PSC+1) = 10KHz
  	TIM12->ARR = period-1;
  	TIM12->CNT = 0;
  	TIM12->EGR = 1;		// update registers;

  	TIM12->SR  = 0;		// clear overflow flag
  	TIM12->CR1 = 1;		// enable Timer6

  	while (!TIM12->SR);
    
  	TIM12->CR1 = 0;		// stop Timer6
  	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM12, DISABLE);
}