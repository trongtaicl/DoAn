
#define CONFIG_H_
#include "stm32f4xx.h"
#include "math.h"
#include "tm_stm32f4_mpu6050.h"

typedef struct{
    int mean_gx[2]; 
	  int gxValue; 
	  int gx_offset; 
	  float angle_roll; 
	  float angle_output;
	  float gyroRate; 
	  TM_MPU6050_t        MPU6050_Data0; 
    TM_MPU6050_Result_t result; 
    uint16_t             bufferSize; 
    uint8_t             gyroDeadZone;  
    uint32_t            numberDiscard; 	
	  uint8_t             state; 
	  uint16_t             count;
	  uint8_t             check; 
	  uint8_t             enabled; 
	
	  float               setAngleDegree;   
	  int                 idealValue; 
	  float               tSample; 
	 
	
}   MPU6050_Device_t;

typedef struct{
	float Q_angle; // Process noise variance for the accelerometer
  float Q_bias; // Process noise variance for the gyro bias
  float R_measure; // Measurement noise variance - this is actually the variance of the measurement noise

  float angle; // The angle calculated by the Kalman filter - part of the 2x1 state vector
  float bias; // The gyro bias calculated by the Kalman filter - part of the 2x1 state vector
  float rate; // Unbiased rate calculated from the rate and the calculated bias - you have to call getAngle to update the rate

  float P[2][2]; // Error covariance matrix - This is a 2x2 matrix
}KalmanStuff_t; 


void Timer3(void); 

void Timer1(void);

void Timer4(void);

void GPIO_Configuration(void);

void Timer2(void);

void Timer5(void); 

void itoa(int s, char* buffer);

void USART_Configuration(unsigned int BaudRate);

void Timer13(void);

