#ifndef USART_H_
#define USART_H_
#include "stm32f4xx_usart.h"
#include "stm32f4xx_dma.h"

void GPIO_init(void);
void UART_init(void);
void Send_uart1(uint16_t dataout);
void USART_puts(const char *s, uint8_t n);

void USART2_Configuration(unsigned int baudRate); 
 
#endif