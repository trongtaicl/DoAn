#include "stm32f4xx.h"
#include "config.h"
#include "motor.h"
#include "math.h"
#include "uart.h"
#include "stdio.h"
#include "stdlib.h"
#include "tm_stm32f4_i2c.h"
#include "tm_stm32f4_mpu6050.h"
#include "stm32f4xx_it.h"

/********************************************************************************
* Macro
********************************************************************************/
#define     BUFFER_RX_SIZE  20
#define     BUFFER_TX_SIZE  22
#define     TSAMPLE         0.1
#define     ENCODERCOUNT    1452
#define     NUMBERELEMENTS  800
#define     TIMEFORCOUNTER  (float)1/(float)40000 
#define     RADIANDEGREE     (float)180/3.141592654
/********************************************************************************
* Variable to get value to view
********************************************************************************/

uint32_t  ENCCount =0, numCount =0; 
int start = 0; 
/************
USART variable
***************/
uint16_t          USARTBuffer[BUFFER_TX_SIZE] = {0};
uint16_t          USARTBufferRX[BUFFER_RX_SIZE] = {0}; 


Robot_t  robot; 
Robot_t* pRobot = &robot; 
/********************************************
Variable for Motor1 
******************************************/
Motor_t motor1;
Motor_t* pMotor1 = &motor1;
Speed_control_t speedParams1;
Position_control_t positionParams1;
motor_profile_t profile1;
control_mode_t  controlMode1; 
pid_selfTurning_t pidParams; 


/****************************
Variable for Motor2
**************************/
Motor_t motor2; 
 Motor_t* pMotor2 = &motor2; 
Speed_control_t speedParams2; 
Position_control_t positionParams2; 
motor_profile_t profile2; 
control_mode_t  controlMode2; 

/******************************
Pointer for usart
******************************/
float   setSpeed =0; 
uint8_t *usartSpeedPointer ; 
uint8_t *usartSpeedReceived = (uint8_t*)&(setSpeed); 


/***************
variable for IMU
*******************/
MPU6050_Device_t MPU6050Dev; 
KalmanStuff_t    kalmanY; 
/************************
Temp Variable
************************/
//uint16_t buffer[2] ={0}; 
//uint8_t i =0, resetCount =0;
//volatile uint8_t test =0; 
//float   tempNum; 
///uint16_t    state = 0; // in case that the second set position, when pMotor->pulseW = 6500, it will stop right away
float   valueAcceY =0; 
uint16_t countNum =0; 
//uint8_t  txBuffer[3] ={0}; 
uint16_t  txCounter = 0; 
//uint8_t  rxBuffer[4] = {0}; 
int16_t rxCounter = 0; 
//uint8_t  enableTx =0; 
uint16_t counterResult = 0;  
volatile int  resultMean =0; 
int      bufferTestGyro2[800] = {0};
uint16_t counterTestGyro = 0; 
//float    bufferTestPosition[200] = {0}; 
float    speedMotor1, speedMotor2; 
/*uint16_t counterTIM1, counterTIM2;
uint16_t tempCount  =0;
uint16_t counterTimer = 0; 
uint16_t counterTimer4 = 0; 
float    tSample;  
uint8_t  startPulse; 
float kalAngleZ =0, kalAngleY =0; // Angle after Kalman filter
float compAngleY =0, gyroYAngle =0; 
float dt; 
float gyroYRate; */
uint8_t initDone =0;
volatile int      resultTemp =0; 
/************************
Axes in cm and buffer to save setPosition
*********************/
float    x= -60.0, y = -60.0;
float    bufferMotor1[2] = {0}; // this variable for saving setPosition of motor1
float    bufferMotor2[2] = {0}; // this variable for saving setPostion of motor2
uint16_t reset =0;             // reset system, reset counter of encoder, stop robot
int gxOffsetTemp;
float    bufferTSample[800] = {0}; 
uint16_t txCounter2 = 0; 
/*******************
function
******************/
void ConfigDMA_USART_RX(void);
void ConfigDMA_USART_TX(void); 
void ConfigInterruptDMA(void); 
float GetValueDegree(float a);
void ProcessAxes( float x, float y, float *setMotor1, float *setMotor2);
float convertToFloat(uint16_t buffer[], uint8_t index);
void assginValueToSend(uint8_t index, uint16_t buffer[], float value);
int meansensors(MPU6050_Device_t *MPU6050);
void calibration(MPU6050_Device_t *MPU6050);
void calculateSensor(TM_MPU6050_t *MPU6050_Data, uint16_t count);
void DMA1_Stream5_IRQHandler(void);
void USART(USART_TypeDef* USARTx,unsigned int BaudRate);
float getAngle(float newAngle, float newRate, float t, KalmanStuff_t kalman);	
float angle = 0;


int main(void)
{
	uint8_t buffer[4] = {0};
	uint16_t k =0; 
	int bufferGyro[800] = {0}; 
	
	/*********
	initialize robot, xPos, yPos is the coordinates of robot. 
	***************/
	pRobot->tSample = 0.03; 
	pRobot->xPos    = 0; 
	pRobot->yPos    = 0; 
	pRobot->count   = 0; 
	pRobot->enabled = 0; 
	/*****************
	initialize pid self-tunning
	*****************/
	pidParams.pE =  0; 
	pidParams.nuyKi = 0;
	pidParams.nuyKp = 0; 
	pidParams.vI1  = 0.001f;
	pidParams.vP1  = 0.001f; 
	
	
	/***************
	initialize for speed parameter 
	*******************/
	speedParams1.Kp = 3;
	speedParams1.Ki = 0.01;
	speedParams1.Kd = 0.001;
	speedParams1.speedCOF = (float)60/(float)(1300*0.1);
	speedParams1.startRPM = 30;
	speedParams1.setSpeed = 50; 
	speedParams1.setPulse = (int)(speedParams1.setSpeed/speedParams1.speedCOF); 
	speedParams1.currentPulse = 0;
	speedParams1.currentSpeed = 0;
	profile1.startPulse = 6000;
	speedParams1.speedCms = 0;
	speedParams1.convertRPMToCms = 0.5117882769f; 
	speedParams1.iPart = 0 ;
	
	/************
	initialize for position
	***************/
	positionParams1.setPosition = 1608; 
	positionParams1.positionEOF = (float)360/(float)1330; 
	positionParams1.Kp          =  2.5; 
	positionParams1.Ki          = 0.01; //0.5;
	positionParams1.Kd          = 0.5; // 0.5; 
	positionParams1.currentPosition = 0.0;
	positionParams1.iPart       = 0; 
	positionParams1.pPart       = 0; 
	positionParams1.dPart       = 0; 
	
	pMotor1->test               = 0; 
	pMotor1->ENC_count[0] =0;
	pMotor1->ENC_count[1] = 0;
	pMotor1->ENC_count[2] = 0;
	pMotor1->positionParams.p_error[0] = 0; 
	pMotor1->positionParams.p_error[1] =0;
	pMotor1->positionParams.p_error[2] =0; 
	pMotor1->pGPIO        = GPIOD; 
	
	// chan 7 voi PD12 (forwart), chan 8 voi PD13 (reverse)
	// chan C2 voi PA15, chan C1 voi PB3
	pMotor1->pGPIOForward = GPIO_Pin_13; 
	pMotor1->pGPIOReverse = GPIO_Pin_12; 
	pMotor1->pTimCCR      = &(TIM1->CCR1);  
	pMotor1->pTimCNT      = &(TIM2->CNT); 
	//pMotor1->countDiscard = 0; 
	pMotor1->PulseW       = 0; 
	
	pMotor1->controlMode = pulseToSpeed;
	Motor_Init(pMotor1, speedParams1, positionParams1, profile1,pidParams); 
	
	
	positionParams2.setPosition = 1608; 
	positionParams2.positionEOF = (float)360/(float)1330; 
  positionParams2.Kp            = 2.7;
	positionParams2.Ki            = 0.01;
	positionParams2.Kd            = 0.5;
	positionParams2.currentPosition = 0;
	/*************
	*** 810 degree *****
	**************/
	
	pMotor2->ENC_count[0] =0;
	pMotor2->ENC_count[1] = 0;
	pMotor2->ENC_count[2] = 0;
	pMotor2->positionParams.p_error[0] = 0; 
	pMotor2->positionParams.p_error[1] =0;
	pMotor2->positionParams.p_error[2] =0; 
	pMotor2->pGPIO        = GPIOD; 
	// chan 9 voi PD15 (reverse), chan 4 voi PD14 (forward)
	// chan C2 voi PA1, chan C1 voi PA0
	//pMotor2->countDiscard = 0; 
	pMotor2->pGPIOForward = GPIO_Pin_15; 
	pMotor2->pGPIOReverse = GPIO_Pin_14; 
	pMotor2->pTimCCR      = &(TIM1->CCR3);  
	pMotor2->pTimCNT      = &(TIM5->CNT); 
	
	pMotor2->controlMode = pulseToSpeed;
	speedParams1.setSpeed = 0; 
	Motor_Init(pMotor2, speedParams1, positionParams2, profile2, pidParams); 
	
	
	kalmanY.Q_angle = 0.001f;
  kalmanY.Q_bias = 0.003f;
  kalmanY.R_measure = 0.03f;

  kalmanY.angle = 0.0f; // Reset the angle
  kalmanY.bias = 0.0f; // Reset bias

  kalmanY.P[0][0] = 0.0f; // Since we assume that the bias is 0 and we know the starting angle (use setAngle), the error covariance matrix is set like so - see: http://en.wikipedia.org/wiki/Kalman_filter#Example_application.2C_technical
  kalmanY.P[0][1] = 0.0f;
  kalmanY.P[1][0] = 0.0f;
  kalmanY.P[1][1] = 0.0f;
	
	GPIO_init(); 
	//UART_init();
	USART2_Configuration(57600); 
	ConfigDMA_USART_TX();
	ConfigDMA_USART_RX(); 
	ConfigInterruptDMA(); 
	Timer2(); 
	Timer3(); 
	Timer5(); 
	Timer1();
	Timer4();
	Timer13(); 
	
	//USART(USART1,9600); 
	
	GPIO_ResetBits(GPIOD, GPIO_Pin_14);
	GPIO_ResetBits(GPIOD, GPIO_Pin_15);
	GPIO_ResetBits(GPIOD, GPIO_Pin_12);
	GPIO_ResetBits(GPIOD, GPIO_Pin_13); 
 
	/**********
	enable for receiving data usart 2
	************/
	USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE);

	//lengthData = sizeof(data)/sizeof(char);
	setSpeed   = 0.123; 
	
	USARTBuffer[0] = (uint16_t)'p'; USARTBuffer[1] = (uint16_t)'o';
	USARTBuffer[2] = (uint16_t)'s'; USARTBuffer[3] = (uint16_t)'t';
	
	/* gyro */
	MPU6050Dev.bufferSize    = 100; 
	MPU6050Dev.gyroDeadZone  = 1; 
	MPU6050Dev.numberDiscard = 0;
	MPU6050Dev.state         = 0; 
	MPU6050Dev.angle_roll    =0; 
	MPU6050Dev.count         =0;
	
	
/*****test usart 2 ************/	
	pMotor1->positionParams.setPosition = 1608; 
	pMotor2->positionParams.setPosition = 1608; 
	/*testBuf[0] = 'A'; 
	testBuf[1] = 'T'; 
  testBuf[2] = '+';
  testBuf[3] = 'R';
  testBuf[4] = 'T';	
*/
	/*************
	test mpu6050 and usart sending angle value to computer
	*********/
	MPU6050Dev.check = 0; 
	MPU6050Dev.mean_gx[0] = 0; 
	MPU6050Dev.mean_gx[1] = 0; 
	ENCCount = 0; 
	//startPulse = 0; 
	
	while((TM_MPU6050_Init(&MPU6050Dev.MPU6050_Data0,TM_MPU6050_Device_0,TM_MPU6050_Accelerometer_8G,
		                     TM_MPU6050_Gyroscope_500s) != TM_MPU6050_Result_Ok))
	{
		delay_01ms(1000); 
		GPIO_SetBits(GPIOD, GPIO_Pin_12); 
	}
	
	TM_MPU6050_setXGyroOffset(&MPU6050Dev.MPU6050_Data0, 0); 
	TM_MPU6050_setYGyroOffset(&MPU6050Dev.MPU6050_Data0,0); 
	TM_MPU6050_setZGyroOffset(&MPU6050Dev.MPU6050_Data0,0);
	
	MPU6050Dev.idealValue = (float)pMotor1->speedParams.setSpeed*180.0*65.5/(17.0*3.14);
	TIM3->CNT = 0;
	pMotor1->controlState = 0; 
  while (1)
  { 		
	
		/*GPIO_ResetBits(pMotor1->pGPIO,pMotor1->pGPIOForward);
	  GPIO_SetBits(pMotor1->pGPIO,pMotor1->pGPIOReverse);
	  GPIO_SetBits(pMotor2->pGPIO, pMotor2->pGPIOForward);
	  GPIO_ResetBits(pMotor2->pGPIO, pMotor2->pGPIOReverse); 
	  *pMotor2->pTimCCR = 00;
	  *pMotor1->pTimCCR = 6600;
		*/
	 if (start ==1)
	 {
				if (initDone == 0)
				{ 
					initDone = 1; 
					TIM13->CR1 = 1; 
					TIM4->CR1  = 1; 
				}
		
        if ((TIM13->CNT > 4000) && (pMotor1->positionParams.currentPosition > pRobot->idealPosition) && (pRobot->enabled ==1 ))
					{
						pRobot->tSample = (float)(TIM13->CNT)*TIMEFORCOUNTER;
						pMotor1->speedParams.speedCOF = (float)60/(float)(1300*pRobot->tSample);
						motor_sampling(pMotor1); 
						rxCounter = pMotor1->positionParams.currentPosition;
						
					/*	if (countNum < NUMBERELEMENTS)
						{
							bufferTestPosition[countNum] = pRobot->tSample;
              bufferTestENC[countNum] = speedMotor1; 							
							countNum++; 
						}*/
											
						GPIO_ResetBits(pMotor1->pGPIO,pMotor1->pGPIOForward);
	          GPIO_SetBits(pMotor1->pGPIO,pMotor1->pGPIOReverse);
	          GPIO_SetBits(pMotor2->pGPIO, pMotor2->pGPIOForward);
	          GPIO_ResetBits(pMotor2->pGPIO, pMotor2->pGPIOReverse); 
	          *pMotor2->pTimCCR = 00;
	          *pMotor1->pTimCCR = 6600; 
						//motor_control(pMotor1, pRobot->tSample); 
						TIM13->CR1 = 0; 
						TIM13->CNT = 0;
						TIM13->CR1 = 1; 
					}
					else if (pMotor1->positionParams.currentPosition < pRobot->idealPosition)
					{
						*pMotor1->pTimCCR = 0;
						*pMotor2->pTimCCR = 0; 
						pMotor1->positionParams.currentPosition = 0; 
						pRobot->enabled = 0; 
						MPU6050Dev.enabled = 1; 
						
					}
					
				if (MPU6050Dev.state==0)
	      {
			     MPU6050Dev.bufferSize = 100; 
			     MPU6050Dev.numberDiscard = 0; 
			     MPU6050Dev.gx_offset = meansensors(&MPU6050Dev);			
           gxOffsetTemp = MPU6050Dev.gx_offset;	
			     MPU6050Dev.state++;
			     delay_01ms(20);			
	      }	
				if (MPU6050Dev.state == 1)
				{
					MPU6050Dev.bufferSize = 1; 
					MPU6050Dev.numberDiscard = 0;  
					
					resultMean  =  meansensors(&MPU6050Dev);
					resultTemp  = resultMean;
					MPU6050Dev.tSample = (float)(TIM3->CNT)*TIMEFORCOUNTER; 
					TIM3->CR1 = 0; 
					TIM3->CNT = 0;
					TIM3->CR1 = 1; 
					/*********************
					xu ly goc 
					********************/
				  if ((resultMean < -2500) || (resultMean > 2500))
					{
						MPU6050Dev.gx_offset = 250; 
					}
					else if ((resultMean < -2000) || (resultMean >2000))
					{
						MPU6050Dev.gx_offset = 200;
					}
					else if ((resultMean < -1500) || (resultMean > 1500))
					{
						MPU6050Dev.gx_offset = 250; 
					}
					else if ((resultMean < -1000) || (resultMean > 1000))
					{
						MPU6050Dev.gx_offset = 250; 
					}
					else if ((resultMean < -500) || (resultMean > 500))
					{
						MPU6050Dev.gx_offset = 344; 
					}
					else 
					{
						MPU6050Dev.gx_offset = 344;
					}
					
					if (abs(resultTemp - MPU6050Dev.gx_offset) >20)
					{
						MPU6050Dev.gyroRate = (resultTemp- MPU6050Dev.gx_offset)/65.5; 
					}
					else
					{
						MPU6050Dev.gyroRate = 0; 
					}

					if (counterResult < NUMBERELEMENTS)
					{
						bufferGyro[counterResult] = resultTemp; 
						counterResult++; 
					}
					else if (counterTestGyro < NUMBERELEMENTS)
						{
							bufferTestGyro2[counterTestGyro++] = resultTemp;
						}

					MPU6050Dev.angle_roll += MPU6050Dev.gyroRate*MPU6050Dev.tSample; 
   
					valueAcceY = MPU6050Dev.angle_roll;
					delay_01ms(40);
				}
				  
          if (TIM4->CNT > 8000)
					{
						if (MPU6050Dev.enabled == 0)
						//if (txCounter < NUMBERELEMENTS)
						{
							assginValueToSend(4, USARTBuffer, valueAcceY);
							// convert float value to 4 bytes unsigned char and save it to a buffer to send. 
              assginValueToSend(8, USARTBuffer, bufferGyro[txCounter++]); 
							assginValueToSend(12, USARTBuffer, pMotor1->ENC_count[0]);
							assginValueToSend(16, USARTBuffer, gxOffsetTemp);
							USARTBuffer[20] = (uint16_t)'o';
							USARTBuffer[21] = (uint16_t)'n';
							USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
							TIM4->CR1 = 0; 
							TIM4->CNT = 0; 
							TIM4->CR1 = 1; 						
						}
						else if (txCounter < 10)
						{
							assginValueToSend(4, USARTBuffer, valueAcceY);
							// convert float value to 4 bytes unsigned char and save it to a buffer to send. 
              assginValueToSend(8, USARTBuffer, bufferTestGyro2[txCounter++]); 
							assginValueToSend(12, USARTBuffer, 12.2);
							assginValueToSend(16, USARTBuffer, gxOffsetTemp);
							USARTBuffer[20] = (uint16_t)'o';
							USARTBuffer[21] = (uint16_t)'n';
							USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
							TIM4->CR1 = 0; 
							TIM4->CNT = 0; 
							TIM4->CR1 = 1; 					
						}
						
					}	
				
				
	 }
  
	 else if (start ==0)
	 {
		GPIO_ResetBits(pMotor1->pGPIO,pMotor1->pGPIOForward);
		GPIO_SetBits(pMotor1->pGPIO,pMotor1->pGPIOReverse);
		GPIO_SetBits(pMotor2->pGPIO, pMotor2->pGPIOForward);
		GPIO_ResetBits(pMotor2->pGPIO, pMotor2->pGPIOReverse); 
		*pMotor2->pTimCCR = 0;
		*pMotor1->pTimCCR = 0;
	 }
	 
	
	}
}

float getAngle(float newAngle, float newRate, float t, KalmanStuff_t kalman)
{
	  float S;
	  float K[2]; // Kalman gain - This is a 2x1 vector
	  float y;
	  float P00_temp;
	  float P01_temp;
	
	  kalman.rate = newRate - kalman.bias;
    kalman.angle += t * kalman.rate;

    // Update estimation error covariance - Project the error covariance ahead
    /* Step 2 */
   // kalman.P[0][0] += t * (dt*kalman.P[1][1] - kalman.P[0][1] - kalman.P[1][0] + kalman.Q_angle);
    kalman.P[0][1] -= t * kalman.P[1][1];
    kalman.P[1][0] -= t * kalman.P[1][1];
    kalman.P[1][1] += kalman.Q_bias * t;

    // Discrete Kalman filter measurement update equations - Measurement Update ("Correct")
    // Calculate Kalman gain - Compute the Kalman gain
    /* Step 4 */
    S = kalman.P[0][0] + kalman.R_measure; // Estimate error
    /* Step 5 */
    
    K[0] = kalman.P[0][0] / S;
    K[1] = kalman.P[1][0] / S;

    // Calculate angle and bias - Update estimate with measurement zk (newAngle)
    /* Step 3 */
    y = newAngle - kalman.angle; // Angle difference
    /* Step 6 */
    kalman.angle += K[0] * y;
    kalman.bias += K[1] * y;

    // Calculate estimation error covariance - Update the error covariance
    /* Step 7 */
    P00_temp = kalman.P[0][0];
    P01_temp = kalman.P[0][1];

    kalman.P[0][0] -= K[0] * P00_temp;
    kalman.P[0][1] -= K[0] * P01_temp;
    kalman.P[1][0] -= K[1] * P00_temp;
    kalman.P[1][1] -= K[1] * P01_temp;

    return kalman.angle;
}

/*
convert 4 bytes unsigned char of buffer to float. 
*********/
float convertToFloat(uint16_t buffer[], uint8_t index)
{
	float temp;
	uint8_t i =0, num =0;
	uint8_t *numPoint = &num; 
	
	for(i =index; i< (index+4); i++)
	{
		*(numPoint++) = (uint8_t)buffer[i];
	}
	temp = *((float*)&num); // chuyen dia chi qua float, roi lay gia tri cua cai float do. 
	return temp; 
}

/***************
convert radian to degree 
***********/
float GetValueDegree(float a)
{
	return a/(float)(2041); 
}

/*******************
based on data that microprocessor receives via usart 2, set the setPosition for two motors. 
*************/
void ProcessAxes( float x, float y, float *setMotor1, float *setMotor2)
	{
		float temp = 0.0;
    float tempActan = 0.0;		
		if (x == 0)
		{
		  *setMotor1 = GetValueDegree((float)(36000*y));  
      *setMotor2 = GetValueDegree((float)(36000*y));			
		}
		
		else
		{
			temp = sqrt(x*x +y*y); 
			
			if ((x >0) &&(y <0))
			{
				tempActan = atan(x/y) - 3.1416/2;
				*setMotor1 = GetValueDegree((float)(12240*tempActan));
			  *setMotor2 = 0.0; 
			}
			else if ((x <0) &&(y <0))
			{
				tempActan = -(atan(x/y) +3.1416/2);
        *setMotor2 = GetValueDegree((float)(12240*tempActan));
			  *setMotor1 = 0.0; 				
			}
			else if ((x<0)&&(y>0))
			{
				tempActan = atan(x/y);
        *setMotor2  = GetValueDegree((float)(24480*tempActan));
			  *setMotor1 = 0.0; 			
			}
			else if ((x>0) && (y>0))
			{
				tempActan    = atan(x/y); 
				*setMotor1   = GetValueDegree((float)(24480*tempActan));
				*setMotor2   = 0.0; 
			}
			
			*(setMotor1+1) =  GetValueDegree((float)(730*temp));  
      *(setMotor2+1) =  GetValueDegree((float)(730*temp));  
		}
	}


void assginValueToSend(uint8_t index, uint16_t buffer[], float value)
{
	uint8_t* tempNum;
	uint8_t i =0; 
	
	tempNum = (uint8_t*)(&value);  // ep kieu dia chi, de point con tro tempNum vao dau vung nho cua value. tang len 1 thi
                                 // se la gia tri byte tiep theo. 	
	for (i =index; i< (4+index); i++)
  {
		buffer[i] = *(tempNum++); 
	}		
}


void USART(USART_TypeDef* USARTx,unsigned int BaudRate)
{
	GPIO_InitTypeDef GPIO_InitStructure; 
	USART_InitTypeDef USART_InitStructure; 
	NVIC_InitTypeDef  NVIC_InitStructure; 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE); 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  
  /* Configure USART Tx as alternate function  */
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Configure USART Rx as alternate function  */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  
  USART_InitStructure.USART_BaudRate = BaudRate;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USARTx, &USART_InitStructure);
  
	if (USARTx == USART1)
	{
		GPIO_PinAFConfig(GPIOB,GPIO_PinSource6,GPIO_AF_USART1); 
		GPIO_PinAFConfig(GPIOB,GPIO_PinSource7,GPIO_AF_USART1); 
	}
  
  USART_Cmd(USARTx, ENABLE);  
	
	if (USARTx == USART1)
	{
		NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn ;
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority =0;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
		USART_ITConfig(USARTx, USART_IT_RXNE,ENABLE); 
	}
}

void ConfigDMA_USART_TX(void)
{
	 DMA_InitTypeDef DMA_InitStruct; 
   
	 DMA_Cmd(DMA1_Stream6,DISABLE);
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)(&(USART2->DR));
	DMA_InitStruct.DMA_PeripheralInc      = DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStruct.DMA_PeripheralBurst    = DMA_PeripheralBurst_Single;
	DMA_InitStruct.DMA_Memory0BaseAddr    = (uint32_t)&USARTBuffer; 
	//DMA_InitStruct.DMA_Memory0BaseAddr    = (uint32_t)&testBuf; 
	DMA_InitStruct.DMA_MemoryInc          = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_MemoryDataSize     = DMA_MemoryDataSize_HalfWord;
	DMA_InitStruct.DMA_MemoryBurst        = DMA_MemoryBurst_Single;
	DMA_InitStruct.DMA_FIFOMode           = DMA_FIFOMode_Disable;
	DMA_InitStruct.DMA_FIFOThreshold      = DMA_FIFOThreshold_HalfFull;
	DMA_InitStruct.DMA_BufferSize         = BUFFER_TX_SIZE ;
	//DMA_InitStruct.DMA_BufferSize         = 5 ;
	DMA_InitStruct.DMA_Channel 						= DMA_Channel_4;
	DMA_InitStruct.DMA_DIR 								= DMA_DIR_MemoryToPeripheral;
	DMA_InitStruct.DMA_Mode 							= DMA_Mode_Normal;
	DMA_InitStruct.DMA_Priority 					= DMA_Priority_High;
	DMA_Init(DMA1_Stream6,&DMA_InitStruct);
	DMA_Cmd(DMA1_Stream6,ENABLE);
	DMA_ITConfig(DMA1_Stream6,DMA_IT_TC,ENABLE);
}

void ConfigDMA_USART_RX(void)
{
	DMA_InitTypeDef DMA_InitStruct;
	
	DMA_Cmd(DMA1_Stream5,DISABLE);
	DMA_InitStruct.DMA_PeripheralBaseAddr   = (uint32_t)(&(USART2->DR));
	DMA_InitStruct.DMA_PeripheralInc 				= DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_PeripheralDataSize 	= DMA_PeripheralDataSize_HalfWord;
	DMA_InitStruct.DMA_PeripheralBurst 			= DMA_PeripheralBurst_Single;
	DMA_InitStruct.DMA_Memory0BaseAddr 			= (uint32_t)&USARTBufferRX;
	DMA_InitStruct.DMA_MemoryInc 						= DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_MemoryDataSize 			= DMA_MemoryDataSize_HalfWord;
	DMA_InitStruct.DMA_MemoryBurst 					= DMA_MemoryBurst_Single;
	DMA_InitStruct.DMA_FIFOMode 						= DMA_FIFOMode_Disable;
	DMA_InitStruct.DMA_FIFOThreshold 				= DMA_FIFOThreshold_HalfFull;
	DMA_InitStruct.DMA_BufferSize 					= BUFFER_RX_SIZE;
	DMA_InitStruct.DMA_Channel 							= DMA_Channel_4;
	DMA_InitStruct.DMA_DIR 									= DMA_DIR_PeripheralToMemory;
	DMA_InitStruct.DMA_Mode 								= DMA_Mode_Normal;
	DMA_InitStruct.DMA_Priority 						= DMA_Priority_High;
	
	DMA_Init(DMA1_Stream5, &DMA_InitStruct);
	DMA_ITConfig(DMA1_Stream5, DMA_IT_TC,ENABLE);
	DMA_Cmd(DMA1_Stream5, ENABLE);
}

void ConfigInterruptDMA(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	
	NVIC_InitStruct.NVIC_IRQChannel = DMA1_Stream5_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel = DMA1_Stream6_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStruct);
	
}

/**************
interrupt to get data from computer 
**************/
void DMA1_Stream5_IRQHandler(void) 
{
	 if(DMA_GetITStatus(DMA1_Stream5,DMA_IT_TCIF5) == SET)
		{
			DMA_ClearITPendingBit(DMA1_Stream5,DMA_IT_TCIF5);
		//	USART_DMACmd(USART2,USART_DMAReq_Rx,DISABLE);
		// if command from computer is stopping the robot
			if ((USARTBufferRX[0] == 's') && (USARTBufferRX[1] == 't') 
					&&(USARTBufferRX[2] == 'o') && (USARTBufferRX[3]  == 'p'))
			{
				start = 0; // start =0 => no calculate pid for motor.  
				initMotorAgain(pMotor1);
				initMotorAgain(pMotor2); 
				TIM_Cmd(TIM4, DISABLE); 
			}
			// if command from computer is starting the robot. 
			if ((USARTBufferRX[0] == 0x31) && (USARTBufferRX[1] == 0x73) 
					&&(USARTBufferRX[2] == 0x61) && (USARTBufferRX[3]  == 0x74))
			{
				x = convertToFloat(USARTBufferRX, 4); // get the float value of coordinates x, y
				y = convertToFloat(USARTBufferRX, 8);
        initMotorAgain(pMotor1);
				initMotorAgain(pMotor2); 				
				start = 1; 
				//TIM_Cmd(TIM3, ENABLE);
				TIM_Cmd(TIM4, ENABLE); 
			}
			if ((USARTBufferRX[0] == 116) && (USARTBufferRX[1] == 101)
				&& (USARTBufferRX[2] == 115) && (USARTBufferRX[3] == 116))
			{
			    pRobot->idealPosition = convertToFloat(USARTBufferRX, 4);
				  pRobot->enabled    = 1; 
				  MPU6050Dev.enabled = 0; 
			}
			
			ConfigDMA_USART_RX();
      USART_DMACmd(USART2,USART_DMAReq_Rx,ENABLE); 
		}
}

/***
interrupt to send data to computer
****/
void DMA1_Stream6_IRQHandler(void)
{  
		if(DMA_GetITStatus(DMA1_Stream6,DMA_IT_TCIF6) == SET)
		{
			DMA_ClearITPendingBit(DMA1_Stream6,DMA_IT_TCIF6);
			USART_DMACmd(USART2,USART_DMAReq_Tx,DISABLE);
			MPU6050Dev.check = 0; 
			ConfigDMA_USART_TX(); 
		}
}

/********
interrupt timer 3
***********/
void TIM3_IRQHandler(void)
{
	uint16_t counterTIM9; 
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{	
			if ((start ==0) && (pRobot->enabled == 1))
		{		
			if (txCounter < 200)
			{
			  USARTBuffer[0] = (uint16_t)'e'; USARTBuffer[1] = (uint16_t)'n';
	      USARTBuffer[2] = (uint16_t)'c'; USARTBuffer[3] = (uint16_t)'o';
				//assginValueToSend(4, USARTBuffer, bufferTestENC[txCounter]);
			// convert float value to 4 bytes unsigned char and save it to a buffer to send. 

//				assginValueToSend(8, USARTBuffer, bufferTestPosition[txCounter]);
				assginValueToSend(12, USARTBuffer, angle); 
				assginValueToSend(16, USARTBuffer, 13.4);
			  txCounter++; 
				USARTBuffer[20] = (uint16_t)'o';
				USARTBuffer[21] = (uint16_t)'n';
			  USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE); 
			}
     }		 
					 
			}	
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update); 
}

void calculateSensor(TM_MPU6050_t *MPU6050_Data, uint16_t count)
{
	int16_t gyroX =0, gyroY =0, gyroZ =0; 
	while(count--)
	{
		TM_MPU6050_ReadAll(MPU6050_Data); 
		gyroX += MPU6050_Data->Gyroscope_X; 
		gyroY += MPU6050_Data->Gyroscope_Y;
		gyroZ += MPU6050_Data->Gyroscope_Z;
		delay_01ms(2);
	}
	MPU6050_Data->Gyroscope_X = gyroX/count; 
	MPU6050_Data->Gyroscope_Y = gyroY/count; 
	MPU6050_Data->Gyroscope_Z = gyroZ/count; 
}
	
int meansensors(MPU6050_Device_t *MPU6050){
  long i=0,buff_ax=0,buff_ay=0,buff_az=0,buff_gx=0,buff_gy=0,buff_gz=0;
	int  result; 

  while (i<(MPU6050->bufferSize + MPU6050->numberDiscard+1)){
    // read raw accel/gyro measurements from device
    TM_MPU6050_ReadAll(&MPU6050->MPU6050_Data0); 
		
    if (i>=MPU6050->numberDiscard && i<(MPU6050->bufferSize+MPU6050->numberDiscard)){ //First 100 measures are discarded
      buff_gx=buff_gx+MPU6050->MPU6050_Data0.Gyroscope_X;
    }
    if (i==(MPU6050->bufferSize+MPU6050->numberDiscard)){
      result=buff_gx/MPU6050->bufferSize;
     
    }
    i++; 
  }
	return result; 
}

void calibration(MPU6050_Device_t *MPU6050){
  
	/*MPU6050->gx_offset=-MPU6050->mean_gx/4;
	
  while (1){
    int ready=0;
		TM_MPU6050_setXGyroOffset(&MPU6050->MPU6050_Data0, MPU6050->gx_offset); 
		
    meansensors(MPU6050);

    if (abs(MPU6050->mean_gx)<= MPU6050->gyroDeadZone) ready++;
		
    else MPU6050->gx_offset= MPU6050->gx_offset-MPU6050->mean_gx/(MPU6050->gyroDeadZone+1);
		
    if (ready==1) break;
  }
	*/
}
#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  while (1)
  {}
}
#endif

